######################################################################
### Various internal and external checks on the posterior
######################################################################

######################################################################

## note etaDrug models between-study variation in drug effects (it
## does not model between-drug variation)
studyEffects <- c("etaInterceptStudy", "alphaStudy", "etaDrug") 

unitEffects <- c("etaIntercept", "alpha")

unitDF <- c("dfInterceptUnit", "dfAlphaUnit")

######################################################################
### Investigate study effects

pdf(file = "StudyEffectPostDist.pdf")

effects <- studyEffects

studyInfo <- data.frame(study = bugsdata$study,
                        studyAge = 2008 - adasData$PY,
                        statType = adasData$STATANAL
                        )


studyInfo <- studyInfo[!duplicated(studyInfo$study), ]
studyInfo$dose <- as.numeric(as.character(studyInfo$dose))
studyInfo$studyAge <- as.numeric(as.character(studyInfo$studyAge))

effectVars <- grep(paste(effects[1],"\\[",sep=""),
                   dimnames(posterior)[[2]], value = TRUE)

## study x parameter-type matrix of posterior medians:
medianEffects <- sapply(effects,
                        function(effect, posterior){
                          effectCols <- grep(paste(effect,"\\[",sep=""),
                                             dimnames(posterior)[[2]])
                          apply(posterior[, effectCols], 2, median)
                        },
                        posterior = posterior
                        )

## study x parameter-type matrix of posterior means:
meanEffects <- sapply(effects,
                      function(effect, posterior){
                        effectCols <- grep(paste(effect,"\\[",sep=""),
                                           dimnames(posterior)[[2]])
                        apply(posterior[, effectCols], 2, mean)
                      },
                      posterior = posterior
                      )

for(i in 1:ncol(medianEffects)) {

  ## medianDF = median(posterior[,dfs[i]])
  plots <- qqhist2(medianEffects[, i],
                   label = paste("median", effects[i]),
                   distribution = qnorm)
  ## function(x) qt(x, df=medianDF))
  print(plots[[1]], position = c(0, 0, 0.525, 1), more = TRUE)
  print(plots[[2]], position = c(0.475, 0, 1, 1), more = FALSE)
  ## Note medianEffects is aligned with a column subset of posterior,
  ## and that column subset is aligned with the unique values of the
  ## 'study' element of bugsdata, which is the same as the unique
  ## values of studyInfo$study
  print(medianEffects[studyInfo$study == 56, i]) # CP-457,920
  print(medianEffects[studyInfo$study == 57, i]) # LEADe
  print(summary(medianEffects[!(studyInfo$study %in% c(56,57)), i] )) # literature summary data
  
}

sigmaStudyCols <- grep("sigmaStudy\\[", dimnames(posterior)[[2]]) 

tau <- apply(1 / posterior[, sigmaStudyCols]^2, 2, median)

plots <- qqhist2(tau, label = paste("median","tauStudy"),
                 distribution = function(x) {
                   qgamma(x, shape = median(posterior[, "aResid"]),
                          rate = median(posterior[, "aResid"] * posterior[, "sigma"]^2))
                 }
                 )
               
print(plots[[1]], position=c(0, 0, 0.525, 1), more = TRUE)
print(plots[[2]], position=c(0.475, 0, 1, 1), more = FALSE)

pairs(medianEffects)

xyplot(medianEffects[1:nrow(studyInfo), "alphaStudy"] ~ studyAge,
       data = studyInfo,
       panel = function(x,y,...){
         panel.xyplot(x,y,...)
         panel.abline(h = 0, lwd = 2, lty = 2)
         panel.lmline(x,y,...)
         print(summary(lm(y~x)))
       },
       xlab=list(label="study age (y)",cex=1.5),
       ylab=list(label="study-specific slope (units/week)", cex=1.5),
       scales=list(cex=1.5), lwd=2, cex=1.2
       )

xyplot(medianEffects[1:nrow(studyInfo), "alphaStudy"] ~ as.factor(statType),
       data = studyInfo,
       panel = function(x,y,...){
         panel.xyplot(x,y,...)
         panel.abline(h = 0, lwd = 2, lty = 2)
       },
       xlab=list(label="statistic type",cex=1.5),
       ylab=list(label="study-specific slope (units/week)", cex=1.5),
       scales=list(cex=1.5, x = list(rot = 45)),
       cex=1.2
       )

dev.off()

### End investigate study effects
######################################################################


######################################################################
### Invesigate unit effects

pdf(file = "UnitEffectPostDist.pdf")

effects <- unitEffects
dfs <- unitDF

unitInfo <- data.frame(study=bugsdata$study,
                       unit=bugsdata$unit,
                       drug=swap(bugsdata$drug, 0:3, 
                         c("placebo", "donepezil", "galantamine",
                           "rivastigmine")),
                       dose=bugsdata$dose
                      )

unitInfo <- unitInfo[!duplicated(unitInfo$unit), ]
unitInfo$dose <- as.numeric(as.character(unitInfo$dose))
unitInfo$adas0 <- as.numeric(as.character(unitInfo$adas0))

medianEffects <- sapply(effects,
                        function(effect, posterior){
                          effectCols <- grep(paste(effect,"\\[", sep=""),
                                             dimnames(posterior)[[2]])
                          apply(posterior[, effectCols],
                                2, median)
                        },
                        posterior = posterior
                        )

for(i in 1:ncol(medianEffects)) { 
  medianDF <- median(posterior[ , dfs[i] ])
  plots <- qqhist2(medianEffects[, i],
                   label = paste("median", effects[i]),
                   distribution = function(x) qt(x, df = medianDF)
                   )
  print(plots[[1]], position = c(0,0,0.525,1), more = TRUE)
  print(plots[[2]], position=c(0.475,0,1,1), more = FALSE)
  
}

for(i in 1:ncol(medianEffects)) {
  medianDF <- median(posterior[, dfs[i] ])
  print(qqmath( ~ medianEffects[1:nrow(unitInfo),i] | drug,
               data = unitInfo,
               prepanel = prepanel.qqmathline, 
               distribution = function(x) qt(x, df = medianDF),
               panel = function(x, distribution){
                 panel.qqmath(x, cex = 1.2, pch = 16, distribution = distribution)
                 panel.qqmathline(x, distribution = distribution, col=3, lwd=3)
               },
               ylab = list(label = effects[i], cex=1.2),
               xlab = list(cex = 1.2),
               scales = list(cex = 1.2),
               ylim = range(medianEffects[1:nrow(unitInfo), i])
               )
        )
}

print(summary(medianEffects[, i])) 


pairs(medianEffects)

for(i in 1:ncol(medianEffects)){

  print(xyplot(medianEffects[1:nrow(unitInfo), i] ~ dose | as.factor(drug),
               data = unitInfo,
               panel = function(x,y,...){
                 panel.xyplot(x,y,...)
                 panel.abline(h = 0, lwd = 2, lty = 2)
                 if(length(unique(x)) > 1) panel.lmline(x,y,...)
               },
               xlab = list(label="dose", cex=1.2),
               ylab = list(label=effects[i], cex=1.2),
               scales=list(x=list(relation = "free"))
               )
        )
	
}

dev.off()

### End investigate unit effects
######################################################################





