model{
 
  for(i in 1:nStudies) {
    
    ## inter-trial variation

    etaInterceptStudy[i] ~ dnorm(0, tauInterceptStudy)
    etaDrug[i] ~ dnorm(0, tauDrug)
    alphaStudy[i] ~ dnorm(alphaMean, tauAlphaStudy)	
    tauStudy[i] ~ dgamma(aResid, bResid)
    sigmaStudy[i] <- 1 / sqrt(tauStudy[i])
    
  }
  
  
  for(i in 1:nUnits) {
    
    ##  "eta-bar" in model description in slides
    etaIntercept[i] ~ dt(etaInterceptStudy[ studyUnit[i] ], 
                         tauInterceptUnitAdj[i], dfInterceptUnit)
    ##  "alpha-bar" in model description in slides
    alpha[i] ~ dt(alphaStudy[ studyUnit[i] ], tauAlphaUnitAdj[i], dfAlphaUnit)
    tauInterceptUnitAdj[i] <- tauInterceptUnit * nPatUnit[i]
    tauAlphaUnitAdj[i] <- tauAlphaUnit * nPatUnit[i]
    
  }

  for(i in 1:nObsSummary) {

    ## "adas-bar" in model description in slides
    adasCFB[i] ~ dnorm(adasCFBMean[i], tau[i])
    
    ## "s2-adas" in model description in slides
    varAdasCFB[i] ~ dgamma(aVar[i], bVar[i])

    ## alpha here is the "alpha-bar" in the slides
    ## etaIntercept here is the etaIntercept-bar" in the slides
    adasCFBMean[i] <- alpha[ unit[i] ] * time[i] + etaIntercept[ unit[i] ]  + ePlacebo[i] + eDrug[i]
    
    ePlacebo[i] <- beta * (exp( -kel * time[i]) - exp( -keq * time[i] ))
    iDrug[i] <- max(1, drug[i])   # prevents syntax error when treatment = placebo
    eDrug[i] <- (1 - equals(drug[i], 0)) * pow(dose[i] / doseRef[ iDrug[i] ], gamma[ iDrug[i] ]) *
      eDelta[ iDrug[i] ] * exp(etaDrug[ study[i] ]) * time[i] / (et50[ iDrug[i] ] + time[i])
    
    tauMarginal[i] <- 1 / ( time[i] * time[i] / tauAlphaUnit + 
                           1 / tauInterceptUnit + 
                           1 / tauStudy[ study[i] ]
                           )
    
    tau[i] <- tauStudy[ study[i] ] * nPat[i]
    
    aVar[i] <- (nPat[i] - 1) / 2
    bVar[i] <- tauMarginal[i] * (nPat[i] - 1) / 2
    
  }
  
### prior distribution

  alphaMean ~ dnorm(0, 1.0E-6)
  ## reparameterization of placebo response in attempt to reduce autocorrelation
  aucPlacebo ~ dunif(0, 100)
  beta <- -aucPlacebo / (1/kel - 1/keq)
  kel ~ dunif(0,2)
  keqMinusKel ~ dunif(0,2)
  keq <- kel + keqMinusKel
 
  for(i in 1:(nDrugs)) {
    ## truncated Emax reparameterization to reduce autocorrelation
    logEStar[i] ~ dnorm(0, 1.0E-6)
    log(eStar[i]) <- logEStar[i]
    b[i] ~ dunif(0,100)
    eDelta[i] <- -(1 + b[i]) * eStar[i] / b[i]
    et50[i] <- tStar / b[i]
    gamma[i] ~ dnorm(0, 1.0E-6)I(0,)
  }
 
  sigma ~ dunif(0, 1.0E4)
  tauResid <- 1 / (sigma * sigma)
  aResid ~ dunif(0, 1.0E4)
  bResid <- aResid / tauResid
  
  omegaInterceptStudy ~ dunif(0, 1.0E4)
  omegaInterceptUnit ~ dunif(0, 1.0E4)
  omegaAlphaStudy ~ dunif(0, 1.0E4)
  omegaAlphaUnit ~ dunif(0, 1.0E4)
  omegaDrug ~ dunif(0, 1.0E4)
  dfInterceptUnit ~ dunif(2, 100)
  dfAlphaUnit ~ dunif(2, 100)
 
  tauInterceptStudy <- 1/(omegaInterceptStudy*omegaInterceptStudy)
  tauInterceptUnit <- 1/(omegaInterceptUnit*omegaInterceptUnit)
  tauAlphaStudy <- 1/(omegaAlphaStudy*omegaAlphaStudy)
  tauAlphaUnit <- 1/(omegaAlphaUnit*omegaAlphaUnit)
  tauDrug <- 1/(omegaDrug*omegaDrug)
  
}
