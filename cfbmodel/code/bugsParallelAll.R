
"bugsParallel" <-
  
  function(data, inits, parameters.to.save, model.file = "model.txt",
           n.chains = 3, n.iter = 2000, n.burnin = floor(n.iter / 2),
           n.thin = max(1, floor(n.chains * (n.iter - n.burnin) / 1000)),
           refresh=100, seed = 314159,
           bin = (n.iter - n.burnin) / n.thin,
           debug = FALSE, DIC = TRUE, digits = 5,
           bugs.directory = "c:/Program Files/WinBUGS14/",
           clearWD = FALSE) {

    ## bugsParallel launches WinBUGS on multiple processors. Each
    ## distributed job runs one MCMC chain.  Current version is
    ## limited to WinBUGS 1.4.* running in a Microsoft Windows
    ## environment using Rmpi.  Many R2WinBUGS components are used.
    
    ## use scan to read in model file as a character vector so it can be passed to nodes
    model <- scan(model.file, what="", sep="\n", blank.lines.skip = F)

    ## create seeds for each WinBUGS chain
    set.seed(seed)
    seeds <- as.integer(runif(n.chains,0,1.0E6))

    runOneChain <- function(i, seeds, data, inits, parameters.to.save, model,
                            n.iter, n.burnin, n.thin, refresh, bin, debug, DIC, digits,
                            bugs.directory, clearWD){

      ## Defining all of the following functions within runOneChain is
      ## an easy way of making all of these helper functions available
      ## on all of the nodes
      
      bugs.chainParallel <- function(data, inits, parameters.to.save,
                                     model, n.iter, n.burnin, n.thin,
                                     seed, refresh, bin, debug, DIC, digits,
                                     bugs.directory, clearWD){
        write(model,"model.txt")
        model.file <- "model.txt"
        if(!(length(data) == 1 &&
             is.vector(data) &&
             is.character(data) &&
             data == "data.txt")) {
          bugs.data(data, dir = getwd(), digits)
        } else if(!file.exists(data)) {
          stop("File data.txt does not exist.")
        }
        bugs.inits(inits, 1, digits)
        if(DIC) parameters.to.save <- c(parameters.to.save, "deviance")
        
        bugs.scriptParallel(parameters.to.save, n.iter, n.burnin, n.thin,
                            model.file, seed=seed, refresh=refresh,
                            debug=debug, is.inits=!is.null(inits),
                            bin = bin, DIC = DIC)
        bugs.runParallel(n.burnin,bugs.directory)
        
        sims <- bugs.simsParallel()
        if(clearWD) {
          file.remove(c("data.txt", "log.odc", "log.txt", "codaIndex.txt",
                        paste("inits", 1:n.chains, ".txt", sep=""),
                        paste("coda", 1:n.chains, ".txt", sep="")))
        }
        return(sims)
      }
      
      bugs.data <- 
        function(data, dir = getwd(), digits = 5){
          if(is.numeric(unlist(data)))
            if(is.R()) {
              write.datafile(lapply(data, formatC, digits = digits, format = "E"), 
                             file.path(dir, "data.txt"))
            }
            else {
              writeDatafileS4(data, towhere = "data.txt")
            }
          else {
            if(is.R()) {
              data.list <- lapply(as.list(data), get, pos = parent.frame(2))
              names(data.list) <- as.list(data)
              write.datafile(lapply(data.list, formatC, digits = digits, format = "E"), 
                             file.path(dir, "data.txt"))
            }
            else {
              data.list <- lapply(as.list(data), get, where = parent.frame(2))
              names(data.list) <- unlist(data)
              writeDatafileS4(data.list, towhere = "data.txt")
            }
          }
        }
      
      if(is.R()){
        ## need some fake functions for codetools
        toSingleS4 <- function(...)
          stop("This function is not intended to be called in R!")
        "writeDatafileS4" <- toSingleS4
      } else {
        
        "writeDatafileS4" <- 
          ## Writes to file "towhere" text defining a list containing
          ## "DATA" in a form compatable with WinBUGS.  Required
          ## arguments: DATA - either a data frame or else a list
          ## consisting of any combination of scalars, vectors, arrays
          ## or data frames (but not lists).  If a list, all list
          ## elements that are not data.frames must be named. Names of
          ## data.frames in DATA are ignored.  Optional arguments:
          ## towhere - file to receive output. Is clipboard by
          ## default, which is convenient for pasting into a WinBUTS
          ## ODC file.  fill - If numeric, number of columns for
          ## output. If FALSE, output will be on one line. If TRUE
          ## (default), number of columns is given by .Options$width.
          ## Value: Text defining a list is output to file "towhere".
          ## Details: The function performs considerable checking of
          ## DATA argument. Since WinBUGS requires numeric input, no
          ## factors or character vectors are allowed. All data must
          ## be named, either as named elements of DATA (if it is a
          ## list) or else using the names given in data frames.  Data
          ## frames may contain matrices.  Arrays of any dimension are
          ## rearranged to be in row-major order, as required by
          ## WinBUGS. Scientific notation is also handled properly.
          ## In particular, the number will consist of a mantissa
          ## _containing a decimal point_ followed by "E", then either
          ## "+" or "-", and finally a _two-digit_ number. S-Plus does
          ## not always provide a decimal point in the mantissa, uses
          ## "e" instead of "E", followed by either a "+" or "-" and
          ## then _three_ digits.  Written by Terry Elrod. Disclaimer:
          ## This function is used at the user's own risk.  Please
          ## send comments to Terry.Elrod@UAlberta.ca.  Revision
          ## history: 2002-11-19. Fixed to handle missing values
          ## properly.

          function(DATA, towhere = "clipboard", fill = TRUE) {
            formatDataS4 <- 
              ## Prepared DATA for input to WinBUGS.
              function(DATA) {
                if(!is.list(DATA))
                  stop("DATA must be a named list or data frame.")
                dlnames <- names(DATA)
                if(is.data.frame(DATA))
                  DATA <- as.list(DATA)
                ## Checking for lists in DATA....
                lind <- sapply(DATA, is.list)
                ## Checking for data frames in DATA....
                dfind <- sapply(DATA, is.data.frame)
                ## Any lists that are not data frames?...
                if(any(lind & !dfind)) stop("DATA may not contain lists.")
                ## Checking for unnamed elements of list that are not data frames....
                if(any(dlnames[!dfind] == ""))
                  stop("When DATA is a list, all its elements that are not data frames must be named.")
                ## Checking for duplicate names....
                dupnames <- unique(dlnames[duplicated(dlnames)])
                if(length(dupnames) > 0)
                  stop(paste("The following names are used more than once in DATA:",
                             paste(dupnames, collapse = ", ")))
                if(any(dfind)) {
                  dataold <- DATA
                  DATA <- vector("list", 0)
                  for(i in seq(along = dataold)) {
                    if(dfind[i])
                      DATA <- c(DATA, as.list(dataold[[i]]))
                    else DATA <- c(DATA, dataold[i])
                  }
                  dataold <- NULL
                }
                dlnames <- names(DATA)
                dupnames <- unique(dlnames[duplicated(dlnames)])
                ## Checking for duplicated names again
                ## (now that columns of data frames are
                ## included)....
                if(length(dupnames) > 0)
                  stop(paste("The following names are used more than once in DATA (at least once within a data frame):",
                             paste(dupnames, collapse = ", ")))
                ## Checking for factors....
                factorind <- sapply(DATA, is.factor)
                if(any(factorind))
                  stop(paste("DATA may not include factors. One or more factor variables were detected:",
                             paste(dlnames[factorind], collapse = ", ")))
                ## Checking for character vectors....
                charind <- sapply(DATA, is.character)
                if(any(charind))
                  stop(paste("WinBUGS does not handle character data. One or more character variables were detected:",
                             paste(dlnames[charind], collapse = ", ")))
                ## Checking for complex vectors....
                complexind <- sapply(DATA, is.complex)
                if(any(complexind))
                  stop(paste("WinBUGS does not handle complex data. One or more complex variables were detected:",
                             paste(dlnames[complexind], collapse = ", ")))
                ## Checking for values farther from zero than 1E+38 (which is limit of single precision)....
                toobigind <- sapply(DATA,
                                    function(x) {
                                      y <- abs(x[!is.na(x)])
                                      any(y[y > 0] > 9.9999999999999998e+37)
                                    }
                                    )
                if(any(toobigind))
                  stop(paste("WinBUGS works in single precision. The following variables contain data outside the range +/-1.0E+38: ",
                             paste(dlnames[toobigind], collapse = ", "),
                             ".\n", sep = "")
                       )
                ## Checking for values in range +/-1.0E-38 (which is limit of single precision)....
                toosmallind <- sapply(DATA,
                                      function(x) {
                                        y <- abs(x[!is.na(x)])
                                        any(y[y > 0] < 9.9999999999999996e-39)
                                      }
                                      )
                n <- length(dlnames)
                data.string <- as.list(rep(NA, n))
                for(i in 1:n) {
                  if(length(DATA[[i]]) == 1) {
                    ac <- toSingleS4(DATA[[i]])
                    data.string[[i]] <- paste(names(DATA)[i], "=",
                                              ac, sep = "")
                    next
                  }
                  if(is.vector(DATA[[i]]) & length(DATA[[i]]) > 1) {
                    ac <- toSingleS4(DATA[[i]])
                    data.string[[i]] <- paste(names(DATA)[i], "=c(",
                                              paste(ac, collapse = ", "), ")", sep = 
                                              "")
                    next
                  }
                  if(is.array(DATA[[i]])) {
                    ac <- toSingleS4(aperm(DATA[[i]]))
                    data.string[[i]] <- paste(names(DATA)[i], 
                                              "= structure(.Data= c(",
                                              paste(ac, collapse = ", "),
                                              "), \n   .Dim=c(",
                                              paste(as.character(dim(DATA[[i]])),
                                                    collapse = ", "),
                                              "))",
                                              sep = "")
                  }
                }
                data.tofile <- paste("list(",
                                     paste(unlist(data.string),
                                           collapse = ", "),
                                     ")",
                                     sep = ""
                                     )
                if(any(toosmallind))
                  warning(paste("WinBUGS works in single precision. The following variables contained nonzero data",
                                "\ninside the range +/-1.0E-38 that were set to zero: ",
                                paste(dlnames[toosmallind], collapse = ", "),
                                ".\n",
                                sep = ""
                                )
                          )
                return(data.tofile)
              }
            rslt <- formatDataS4(DATA)
            cat(rslt, file = towhere, fill = fill)
            invisible(0)
          }
        
        
        toSingleS4 <-
          ## Disclaimer: This function is used at the user's own risk. 
          ## Please send comments to Terry.Elrod@UAlberta.ca.
          ## Revision history: 2002-11-19. Fixed to handle missing values properly.
          function(x) {
            xdim <- dim(x)
            x <- as.character(as.single(x))
            
            ## First to look for positives:
            pplus <- regMatchPos(x, "e\\+0")
            pplusind <- apply(pplus, 1, function(y)
                              (!any(is.na(y))))
            if(any(pplusind)) {
              ## Making sure that periods are in mantissa...
              init <- substring(x[pplusind], 1, pplus[pplusind, 1] - 1)
              ## ...preceeding exponent
              pper <- regMatchPos(init, "\\.")
              pperind <- apply(pper, 1, function(y)
                               (all(is.na(y))))
              if(any(pperind))
                init[pperind] <- paste(init[pperind],
                                       ".0", sep = "")
              ## Changing the format of the exponent...
              x[pplusind] <- paste(init, "E+",
                                   substring(x[pplusind],
                                             pplus[pplusind, 2] + 1),
                                   sep = "")
            }
            ## Then to look for negatives:
            pminus <- regMatchPos(x, "e\\-0")
            pminusind <- apply(pminus, 1, function(y)
                               (!any(is.na(y))))
            if(any(pminusind)) {
              ## Making sure that periods are in mantissa...
              init <- substring(x[pminusind], 1,
                                pminus[pminusind, 1] - 1)
              ##...preceeding exponent
              pper <- regMatchPos(init, "\\.")
              pperind <- apply(pper, 1, function(y)
                               (all(is.na(y))))
              if(any(pperind))
                init[pperind] <- paste(init[pperind],
                                       ".0", sep = "")
              ## Changing the format of the exponent...
              x[pminusind] <- paste(init, "E-",
                                    substring(x[pminusind],
                                              pminus[pminusind, 2] + 1),
                                    sep = "")
            }
            x
          }
        
      }
      
      "bugs.inits" <- 
        function (inits, n.chains, digits){
          if(!is.null(inits)) {
            for(i in 1:n.chains) {
              if(is.function(inits))
                if(is.R()) {
                  write.datafile(lapply(inits(), formatC, digits = digits, format = "E"),
                                 paste("inits", i, ".txt", sep = ""))
                } else {
                  writeDatafileS4(inits(),
                                  towhere = paste("inits", i, ".txt", sep = ""))
                }
              else if(is.R()) {
                write.datafile(lapply(inits[[i]], formatC, digits = digits, format = "E"), 
                               paste("inits", i, ".txt", sep = ""))
              } else {
                writeDatafileS4(inits[[i]],
                                towhere = paste("inits", i, ".txt", sep = ""))
              }
            }
          }
        }
      
      "write.datafile" <-
        function (datalist, towhere, fill = TRUE){
          if (!is.list(datalist) || is.data.frame(datalist)) 
            stop("First argument to write.datafile must be a list.")
          cat(formatdata(datalist), file = towhere, fill = fill)
        }
      
      "bugs.scriptParallel" <-
        function (parameters.to.save, n.iter, n.burnin,
                  n.thin, model.file, seed=NULL, refresh=100, debug=FALSE, is.inits, bin, DIC = FALSE){
          ## Write file script.txt for Bugs to readhistory
          
          if((ceiling(n.iter/n.thin) - ceiling(n.burnin/n.thin)) < 2)
            stop ("(n.iter-n.burnin)/n.thin must be at least 2")
          working.directory <- getwd()
          script <- "script.txt"
          model <- if(length(grep("\\\\", model.file)) || length(grep("/", model.file))){
            model.file
          } else file.path(working.directory, model.file)
          data <- file.path(working.directory, "data.txt")
          ##      history <- file.path(working.directory, "history.odc")
          coda  <- file.path(working.directory, "coda")
          logfile <- file.path(working.directory, "log.odc")
          ##      logfileTxt <- file.path(working.directory, "log.txt")
          inits <- sapply(paste(working.directory, "/inits", 1, ".txt", sep=""), 
                          function(x) native2win(x))
          initlist <- paste("inits (", 1, ", '", inits, "')\n", sep="")
          savelist <- paste("set (", parameters.to.save, ")\n", sep="")
          redo <- ceiling((n.iter-n.burnin)/(n.thin*bin))
          
          thinUpdateCommand <- paste("thin.updater (", n.thin, ")\n",
                                     "update (", ceiling(n.burnin/n.thin), ")\n",
                                     sep = "")
          
          cat("display ('log')\n",
              "check ('", native2win(model), "')\n",
              if(!is.null(seed)) paste("set.seed(",as.integer(seed),")\n",sep=""),
              "data ('", native2win(data), "')\n",
              "compile (", 1, ")\n",
              if(is.inits) initlist,
              "gen.inits()\n",
              "refresh(",refresh,")\n",
              thinUpdateCommand,
              savelist,
              if(DIC) "dic.set()\n",
              rep(c("update (", formatC(ceiling(bin), format = "d"), ")\n",
                    "coda (*, '", native2win(coda), "')\n"),
                  redo
                  ),
              ##        "stats (*)\n",
              if(DIC) "dic.stats()\n",
              ##        "history (*, '", native2win(history), "')\n",
              "save ('", native2win(logfile), "')\n", 
              ##        "save ('", native2win(logfileTxt), "')\n",
              file = script,
              sep="",
              append=FALSE
              )
          if (!debug) cat ("quit ()\n", file=script, append=TRUE)
          sims.files <- paste ("coda", 1, ".txt", sep="")
          cat ("WinBUGS did not run correctly.\n",
               file=sims.files, append=FALSE)
        }
      
      "bugs.runParallel" <-
        function(n.burnin, bugs.directory){ 
          
          ## Update the lengths of the adaptive phases in the Bugs updaters
          try(bugs.update.settings(n.burnin, bugs.directory))
          ## Return the lengths of the adaptive phases to their original settings
          on.exit(try(file.copy(file.path(bugs.directory, "System/Rsrc/Registry_Rsave.odc"), 
                                file.path(bugs.directory, "System/Rsrc/Registry.odc"), 
                                overwrite = TRUE)))
          
          ## Search Win*.exe (WinBUGS executable) within bugs.directory
          dos.location <- file.path(bugs.directory, 
                                    grep("^Win[[:alnum:]]*[.]exe$", list.files(bugs.directory), value = TRUE)[1])
          if(!file.exists(dos.location)) 
            stop(paste("WinBUGS executable does not exist in", bugs.directory))
          ## Call Bugs and have it run with script.txt
          bugsCall <- paste("\"", dos.location, "\" /par \"",
                            native2win(file.path(getwd(), "script.txt")),
                            "\"", sep = "")
          temp <- system(bugsCall)
          
          if(temp == -1)
            stop("Error in bugs.run().\nCheck that WinBUGS is in the specified directory.")
          ## Stop and print an error message if Bugs did not run correctly
          if(length(grep("Bugs did not run correctly",
                         scan("coda1.txt", character(), quiet=TRUE, sep="\n"))) > 0)
            stop("Look at the log file and\ntry again with debug=TRUE and figure out what went wrong within Bugs.")
        }
      
      "bugs.update.settings" <-
        function (n.burnin, bugs.directory){
          
          char.burnin <- as.character(n.burnin - 1)
          if (is.R()){
            file.copy(file.path(bugs.directory, "System/Rsrc/Registry.odc"),
                      file.path(bugs.directory, "System/Rsrc/Registry_Rsave.odc"),
                      overwrite = TRUE)
          } else {
            splus.file.copy(file.path(bugs.directory, "System/Rsrc/Registry.odc"),
                            file.path(bugs.directory, "System/Rsrc/Registry_Rsave.odc"),
                            overwrite = TRUE)
          }
          registry <- readBin(file.path(bugs.directory, "System/Rsrc/Registry.odc"), 
                              "character", 400, size = 1, endian = "little")
          locale <- Sys.getlocale("LC_CTYPE")
          Sys.setlocale("LC_CTYPE", "C")
          if (is.R())  
            info <- registry[regexpr("Int", registry, fixed = TRUE, useBytes = TRUE) > 0]
          else  
            info <- registry[regexpr("Int", registry, fixed = TRUE) > 0]
          while(regexpr("\r", info) > 0){
            newline <- regexpr("\r", info)
            info <- substring(info, newline + 1)
            line <- substring(info, 1, regexpr("\r", info) - 1)
            if(regexpr("AdaptivePhase", line) > 0){
              if (is.R())
                numpos <- regexpr("Int", line, fixed = TRUE, useBytes = TRUE) + 4
              else
                numpos <- regexpr("Int", line, fixed = TRUE) + 4
              
              num <- substring(line, numpos)
              if (as.numeric(num) > n.burnin){
                blanks <- rep(" ", nchar(num, type = "chars") - nchar(char.burnin, type = "chars"))
                num.new <- paste(paste(blanks, collapse = ""), char.burnin, sep = "")
                line.new <- sub(num, num.new, line)
                registry <- sub(line, line.new, registry)
              }
            }
          }
          Sys.setlocale("LC_CTYPE", locale)
          writeBin (registry,
                    file.path(bugs.directory, "System/Rsrc/Registry.odc"), endian = "little")
        }
      
      "splus.file.copy"<-
        function(from, to, overwrite = FALSE)
          {
            if(!file.exists(from))
              stop("File: ", from, " does not exist")
            if(!overwrite && file.exists(to))
              stop("File: ", to, " already exists and overwrite is FALSE")
            n <- file.info(from)$size
            z <- writeBin(readBin(from, what = "integer", size = 1, n = n), to,
                          size = 1)
            invisible(z)
          }
      
      "bugs.simsParallel" <-
        function (){
          ## Read the simulations from Bugs into R, format them, and monitor convergence
          
          sims.files <- paste ("coda", 1, ".txt", sep="")
          index <- read.table("codaIndex.txt", header = FALSE, sep = "\t")    # read in the names of the parameters and the indices of their samples
          parameter.names <- as.vector(index[, 1])
          n.keep <- index[1, 3] - index[1, 2] + 1
          n.parameters <- length(parameter.names)
          n.sims <- n.keep
          ## sims <- matrix( , n.sims, n.parameters)
          
          sims <- scan(sims.files, what=list(NULL,""), quiet = TRUE)[[2]][(1:(n.keep * n.parameters))]
          sims <- matrix(as.numeric(ifelse(sims=="inf","Inf",ifelse(sims=="-inf","-Inf",sims))), n.sims, n.parameters)
          ##      sims <- matrix(scan(sims.files, quiet = TRUE)[2 * (1:(n.keep * n.parameters))], n.sims, n.parameters)
          dimnames (sims) <- list (NULL, parameter.names)
          return(sims)
        }
      
      ## from Jun Yan's rbugs package, extended
      
      if (is.R()){
        
        ## get drive mapping table from ~/.wine/config
        winedriveMap <- function(config="~/.wine/config") {
          if (!file.exists(config)) return (NULL);
          con <- readLines(config)
          con <- con[- grep("^;", con)]
          drive <- con[grep("^\\[Drive ", con)]
          drive <- substr(drive, 8, 8)
          drive <- paste(drive, ":", sep="")
          path <- con[grep("Path", con)]
          len <- length(drive)
          path <- path[1:len]
          dir <- sapply(path, 
                        function(x) {
                          foo <- unlist(strsplit(x, "\""))
                          foo[length(foo)]
                        })
          dir <- sub("%HOME%",tools::file_path_as_absolute("~"),dir)
          data.frame(drive = I(drive), path = I(dir), row.names=NULL)
        }
        
        ## translate windows dir to native dir
        winedriveTr <- function(windir, DriveTable=winedriveMap()) {
          win.dr <- substr(windir, 1, 2)
          ind <- pmatch(toupper(win.dr), DriveTable$drive)
          native.dr <- DriveTable$path[ind]
          sub(win.dr, native.dr, windir)
        }
        
        ## translate full Unix path to Wine path
        winedriveRTr <- function(unixpath, DriveTable=winedriveMap()) {
          blocks <- strsplit(unixpath,"/")[[1]]
          cblocks <- c("/",sapply(1+seq(along=blocks[-1]),
                                  function(n) paste(blocks[1:n],collapse="/")))
          path <- match(cblocks,DriveTable$path)
          if (any(!is.na(path))) {
            unixdir <- cblocks[which.min(path)]
            windrive <- paste(DriveTable$drive[min(path,na.rm=TRUE)],"/",sep="")
            winpath <- sub("//","/",sub(unixdir,windrive,unixpath)) ## kluge
          } else stop("can't find equivalent Windows path: file may be inaccessible")
          winpath
        }
        
        
        win2native <- function(x, useWINE=.Platform$OS.type != "windows") { # win -> native
          if (useWINE) winedriveTr(x)
          else x
        }
        
      } # end if (is.R())
      
      native2win <- function(x, useWINE=.Platform$OS.type != "windows", newWINE=TRUE, WINEPATH=NULL) { # native -> win
        if(is.R()){
          if (useWINE && !newWINE) return(winedriveRTr(x))
          if (useWINE && newWINE) {
            x <- system(paste(WINEPATH, "-w", x), intern = TRUE)
            return(gsub("\\\\", "/", x)) ## under wine BUGS cannot use \ or \\
          } else x
        } else { #S-PLUS
          gsub("\\\\", "/", x)  
        }
      }
      
      "formatdata" <-
        function (datalist){
          if (!is.list(datalist) || is.data.frame(datalist)) 
            stop("Argument to formatdata() must be a list.")
          n <- length(datalist)
          datalist.string <- vector(n, mode = "list")
          for (i in 1:n) {
            if (length(datalist[[i]]) == 1) 
              datalist.string[[i]] <- paste(names(datalist)[i], 
                                            "=", as.character(datalist[[i]]), sep = "")
            if (is.vector(datalist[[i]]) && length(datalist[[i]]) > 1) 
              datalist.string[[i]] <- paste(names(datalist)[i], 
                                            "=c(", paste(as.character(datalist[[i]]), collapse = ", "), 
                                            ")", sep = "")
            if (is.array(datalist[[i]])) 
              datalist.string[[i]] <- paste(names(datalist)[i], 
                                            "= structure(.Data= c(", paste(as.character(as.vector(aperm(datalist[[i]]))), 
                                                           collapse = ", "), "), .Dim=c(", paste(as.character(dim(datalist[[i]])), 
                                                                                   collapse = ", "), "))", sep = "")
          }
          datalist.tofile <- paste("list(", paste(unlist(datalist.string), 
                                                  collapse = ", "), ")", sep = "")
          return(datalist.tofile)
        }
      
      myshell <- function(cmd, show.output.on.console = FALSE){
        ## runs DOS command "cmd" without opening a command window
        system(paste(Sys.getenv("COMSPEC"),"/c",cmd),
               intern = FALSE, wait = TRUE, input = NULL,
               show.output.on.console = show.output.on.console,
               minimized = FALSE, invisible = TRUE)
      }

      wd.orig <- getwd()
      wd <- paste(wd.orig,"/",i,sep="")
      dir.create(wd)
      
      setwd(wd); on.exit(setwd(wd.orig))
      
      new.bugs.directory <- file.path(wd, "winbugs14")
      old.bugs.directory <- file.path(dirname(bugs.direct
                                              ory),  # removes final path separator, 
                                      basename(bugs.directory)) # if there is one.
        
      cmd <- paste("xcopy /E/I/Q",
                   shQuote(old.bugs.directory),
                   shQuote(new.bugs.directory)
                   )
           
      shellExe <- myshell(cmd)
      if(shellExe != 0) { # 0 indicates success in this context
        stop("Shell command did not complete successfully")
      }
      
      sims <- bugs.chainParallel(data, inits, parameters.to.save, model,
                                 n.iter, n.burnin, n.thin, seeds[i],
                                 refresh, bin, debug, DIC, digits,
                                 bugs.directory = new.bugs.directory,
                                 clearWD = clearWD)

     if(clearWD) {
        myshell(paste("rd /S/Q", shQuote(wd)))
      } else {
        myshell(paste("rd /S/Q", shQuote(new.bugs.directory)))
      }
      
      sims
      
    }
    
    ## distribute chains to nodes 
    mpi.applyLB(1:n.chains, runOneChain,
                seeds = seeds, data = data, inits = inits,
                parameters.to.save = parameters.to.save, model = model,
                n.iter = n.iter, n.burnin = n.burnin, n.thin = n.thin,
                refresh = refresh, bin = bin, debug = debug, DIC = DIC,
                digits = digits,
                bugs.directory = bugs.directory, clearWD = clearWD)

  }


