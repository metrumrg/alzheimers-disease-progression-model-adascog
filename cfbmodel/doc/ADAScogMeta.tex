\documentclass[12pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}

\usepackage{Sweave}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\oddsidemargin -0.5in
\evensidemargin -0.5in

\newcommand{\adasCfb}{\Delta \mathit{ ADAS}}


\begin{document}
%\section{}
%\subsection{}


\title{ADAS-cog change from baseline model implemented as ``simplemodel''}
\maketitle

\parskip 12pt
\parindent 0em

\section*{Notation}

We index patients by $p$ and within each patient we index occasion by $i$. Where it is convenient, we will use $j = j(p)$, $k = k(p)$, and $d = d(p)$ to denote the indices of the study arm, study, and drug (if applicable) respectively, of patient $p$. Let $\adasCfb_{ipk}$ denote the observed ADAS-cog change from baseline on the $i^{th}$ occasion in the $p^{th}$ patient in the $k^{th}$ study. We will indicated conditioning as follows:
\begin{itemize}
\item $[\, \cdot \, | \mbox{ study } k \,]$ indicates conditioning on all study-level random effects associated with study $k$, {\it without conditioning on patient-level random effects}. 
\item $[\, \cdot \, | \mbox{ patient } p \,]$ indicates conditioning on all patient-level random effects associated with patient $p$ {\it and all study-level random effects associated with study} $k(p)$. (Note that per our model hierarchy, conditioning on patient-level effects implies conditioning on the associated study-level random effects.)
\item $[\, \cdot \, | \mbox{ arm } j \,]$ indicates conditioning on all patient-level random effects for all patients in study arm $j$ {\it and all study-level random effects associated with that study}. That is, $[ \, \cdot \, | \mbox{ arm } j \,] \equiv [ \, \cdot \, | \{ \mbox{ patient } p : j(p) = j \} \,] $
\end{itemize}

\section*{Functions of fixed effect parameters}

\begin{eqnarray}
E_{{\rm placebo},ipk} &=& \beta \left(e^{-k_{el}t_{ijk}} - e^{-k_{eq}t_{ijk}}\right) 
\end{eqnarray}

\begin{Sinput}
for(i in 1:nObsSummary) {

 ePlacebo[i] <- beta * (exp( -kel * time[i]) - exp( -keq * time[i] ))
   
}
\end{Sinput}
  

\section*{Inter-study random effects}

\begin{eqnarray}
\alpha_{{\rm study}, k} & \sim & {\rm N} \left( \mu_{\alpha} , \psi^2_{\alpha} \right) \\
\eta_{{\rm intercept, study}, k} & \sim & {\rm N} \left( 0 , \psi^2_{\rm intercept} \right) \\
\eta_{{\rm drug, study}, k} & \sim & {\rm N} \left( 0 , \psi^2_{\rm drug} \right) \\
1 / \sigma^2_k & \sim & {\rm Gamma} \left( \alpha_{\sigma}, \alpha_{\sigma} \mu^2_{\sigma}  \right) \\
E_{{\rm drug},idk} &=& \left(\frac{D_{d}}{D_{{\rm ref},d}}\right)^{\gamma_{d}} 
	\frac{E_{\Delta,d} e^{\eta_{{\rm drug, study},k}} t_{ijk}}{ET_{50,d} + t_{ijk}} 
\end{eqnarray}

\begin{Sinput}
for(i in 1:nStudies) {
  
  alphaStudy[i] ~ dnorm(alphaMean, tauAlphaStudy)	
  etaInterceptStudy[i] ~ dnorm(0, tauInterceptStudy)
  etaDrug[i] ~ dnorm(0, tauDrug)
  tauStudy[i] ~ dgamma(aResid, bResid)
  sigmaStudy[i] <- 1 / sqrt(tauStudy[i])
  
}
\end{Sinput}

\begin{Sinput}
for(i in 1:nObsSummary) {
  
  iDrug[i] <- max(1, drug[i])   # prevents syntax error when treatment = placebo
  eDrug[i] <- (1 - equals(drug[i], 0)) * pow(dose[i] / doseRef[ iDrug[i] ], gamma[ iDrug[i] ]) *
                     eDelta[ iDrug[i] ] * exp(etaDrug[ study[i] ]) * time[i] / (et50[ iDrug[i] ] + time[i])
  
}
\end{Sinput}


\section*{Inter-patient random effects}

\begin{eqnarray}
\alpha_{pk} \, | \mbox{ study } k &\sim& {\rm T} \left(\alpha_{{\rm study},k}, \, \omega^2_{\alpha},  \, df_{\alpha}\right) \\
\eta_{{\rm intercept} pk} \, | \mbox{ study } k &\sim& {\rm T} \left(\eta_{{\rm intercept, study},k}, \, \omega^2_{\rm intercept},  \, df_{\rm intercept}\right) \\
\end{eqnarray}

\begin{Sinput}
    
## In this version of the model (which uses summary data 
## only), inter-patient random effects are not directly 
## simulated.
    
\end{Sinput}

\section*{Distribution of individual patient scores}

\begin{eqnarray}
\adasCfb_{ipk} \, | \mbox{ patient } p & \sim & {\rm N} \left( {\rm E}[\adasCfb_{ipk} \, | \mbox{ patient } p ] , \, \sigma_k^2\right), \mbox{ where, }   \\
{\rm E}[ \adasCfb_{ipk} \, | \mbox{ patient } p ] & = & \alpha_{pk} t_{ipk} + \eta_{{\rm intercept},pk} + E_{{\rm placebo},ipk} + E_{{\rm drug},ipk} \\
\end{eqnarray}

\begin{Sinput}
    
## In this version of the model (which uses summary data 
## only), there are no observed individual patient scores.
    
\end{Sinput}

\section*{Distribution of sample statistics}

The approximate distribution of summary statistics (specifically, sample means and sample standard deviations, computed over multiple patients) can be derived theoretically based on assumptions already made.

\subsection*{Sampling Theory}

If the patient-level random effects were Normally distributed, it would then follow that  $ [\adasCfb_{ipk} \, | \mbox{ study } k ]$  would be Normally distributed as well. As the patient-level random effects are instead $T$ distributed, this does not hold exactly.  

The mean and variance of $ [\adasCfb_{ipk} \, | \mbox{ study } k ] $ are:

\begin{eqnarray}
 {\rm E}[ \adasCfb_{ipk} \, | \mbox{ study } k] & = & {\rm E} \left[ {\rm E}[ \adasCfb_{ipk} \, | \mbox{ patient } p] \, | \mbox{ study } k \right] \\
& = &\alpha_{{\rm study}, k} t_{ipk} + \eta_{{\rm intercept, study}, k} + E_{{\rm placebo},ijk} + E_{{\rm drug},ijk}  
\end{eqnarray}

\begin{eqnarray}
{\rm V}[ \adasCfb_{ipk} | \mbox{ study } k] & = &  {\rm V} \left[ {\rm E}[ \adasCfb_{ipk} \, | \mbox{ patient } p] \, | \mbox{ study } k \right] \\
&& + {\rm E} \left[ {\rm V}[ \adasCfb_{ipk} \, | \mbox{ patient } p] \, | \mbox{ study } k \right] \\
&= & t_{ijk}^2 \omega_{\alpha}^2  +  \omega^2_{\rm intercept} + \sigma_k^2
\end{eqnarray}

For less obtrusive notation, we will use $\sigma_{{\rm marginal},ijk}^2 \equiv {\rm V}[ \adasCfb_{ijk} | \mbox{ study } k]$. One should bear in mind that ``marginal'' in this context means marginal with respect to patients, but not with respect to study.  

Let $\overline{\adasCfb}_{ijk}$ and $s^2\left(\Delta \mathit{ADAS}\right)_{ijk}$ denote the observed sample mean and observed sample standard deviation of ADAS-cog changes from baseline on the $i^{th}$ occasion in the $j^{th}$ study arm in the $k^{th}$ study. 

\subsection*{Distribution of the sample standard deviation}

The distribution $[ s^2\left(\Delta \mathit{ADAS}\right)_{ijk} \, | \mbox{ arm } j \, ]$ is unecessary for modeling purposes, as we can easily model $[ s^2\left(\Delta \mathit{ADAS}\right)_{ijk} \, | \mbox{ study } k \,]$ via a direct marginal specification (conditional only on study). If the marginal distribution $[ \adasCfb_{ijk} | \mbox{ study } k]$ were exactly Normal, $[ (n_{ijk} - 1) s^2\left(\Delta \mathit{ADAS}\right)_{ijk} / \sigma_{{\rm marginal},ijk}^2 \, | \mbox{ study } k \, ]$ would follow a $\chi^2$ distribution with $n_{ijk} - 1$ degrees of freedom, implying:

\begin{eqnarray}
{\rm E}[ s^2\left(\Delta \mathit{ADAS}\right)_{ijk} \, | \mbox{ study } k \,] & = & \sigma_{{\rm marginal},ijk}^2 \\
{\rm V}[ s^2\left(\Delta \mathit{ADAS}\right)_{ijk} \, | \mbox{ study } k \,] & = & 2 (\sigma_{{\rm marginal},ijk}^2)^2 / (n_{jk} - 1)
\end{eqnarray}

An equivalent way of specifying this distribution is:

\begin{eqnarray}
s^2\left(\Delta \mathit{ADAS}\right)_{ijk} \, | \mbox{ study } k & \sim & {\rm Gamma} \left(\frac{n_{jk} - 1}{2}, \frac{n_{jk} - 1}{2 \sigma_{{\rm marginal},ijk}^2}\right) 
\end{eqnarray}

\begin{Sinput}
for(i in 1:nObsSummary) {

  varAdasCFB[i] ~ dgamma(aVar[i], bVar[i])
    
  aVar[i] <- (nPat[i] - 1) / 2
  bVar[i] <- tauMarginal[i] * (nPat[i] - 1) / 2
  
  tauMarginal[i] <- 1 / ( time[i] * time[i] / tauAlphaUnit + 
                              1 / tauInterceptUnit + 
                              1 / tauStudy[ study[i] ]
  )
  
}
\end{Sinput}
  

\subsection*{Distribution of the sample mean}

Whereas we have described the distribution of $s^2\left(\Delta \mathit{ADAS}\right)_{ijk}$ using a direct marginal specification (conditional only on study), we will use a hierarchical specification for $\overline{\adasCfb}_{ijk}$. To this end, we introduce the following:

\begin{eqnarray}
\overline{\alpha}_{jk} & = & \frac{1}{n_{jk}}   \sum_{p : j(p) = j } \alpha_{pk} \\
\overline{\eta}_{ {\rm intercept}, jk } & = & \frac{1}{n_{jk}} \sum_{p : j(p) = j } \eta_{ {\rm intercept}, pk }.
\end{eqnarray}

It follows (exactly) from assumptions already made that: 

\begin{eqnarray}
\overline{\adasCfb}_{ijk} \, | \mbox{ arm } j & \sim & {\rm N} \left( {\rm E}[ \overline{\adasCfb}_{ijk} \, | \mbox{ arm } j \,] , \, \sigma_{k}^2 / n_{jk} \right) \mbox{ where,} \\
{\rm E}[ \overline{\adasCfb}_{ijk} \, | \mbox{ arm } j \,] & = & \overline{\alpha}_{jk} t_{ijk} + \overline{\eta}_{ {\rm intercept}, jk } +  E_{{\rm placebo},ijk} + E_{{\rm drug},ijk}.  
\end{eqnarray}

\begin{Sinput}
for(i in 1:nObsSummary) {

   adasCFB[i] ~ dnorm(adasCFBMean[i], tau[i])
    
   adasCFBMean[i] <- alpha[ unit[i] ] * time[i] + etaIntercept[ unit[i] ]  + ePlacebo[i] + eDrug[i]
    
   tau[i] <- tauStudy[ study[i] ] * nPat[i]
    
}
\end{Sinput}
  

\subsection*{``Shorthand'' random effects}

In theory one could simulate all of the patient-level random effects $\alpha_{pk}$ and $\eta_{ {\rm intercept}, pk }$ that are required to describe the distribution of $[ \overline{\adasCfb}_{ijk} \, | \mbox{ arm } j \,] $. This would be extravagant, since we would be simulating all of those random effects only to compute their mean, whose distribution should be well-approximated by the Central Limit Theorem (as long as each arm consists of a reasonable number of patients). Accordingly, we introduce the following ``shorthand'' random effects, modeling them according to their asymptotic distributions:

\begin{eqnarray}
\overline{\alpha}_{jk} \, | \mbox{ study } k & \sim & {\rm N}( \alpha_{ {\rm study}, k}, \, \omega^2_{\alpha} / n_{jk} )\\
\overline{\eta}_{ {\rm intercept}, jk } \, | \mbox{ study } k & \sim & {\rm N}( \eta_{ {\rm intercept, study}, k}, \, \omega^2_{\rm intercept} / n_{jk} ) 
\end{eqnarray}

\begin{Sinput}

## For this model and data set, 
## it would be equivalent to loop over 1:nObsSummary

for(i in 1:nUnits) {

  alpha[i] ~ dnorm(alphaStudy[ studyUnit[i] ], tauAlphaUnitAdj[i])
  tauAlphaUnitAdj[i] <- tauAlphaUnit * nPatUnit[i]  

  etaIntercept[i] ~ dnorm(etaInterceptStudy[ studyUnit[i] ], tauInterceptUnitAdj[i])
  tauInterceptUnitAdj[i] <- tauInterceptUnit * nPatUnit[i]

}
\end{Sinput}

\section*{Priors}

\subsection*{Parameters varying by drug}

\begin{eqnarray*}
E_{\Delta,\rm drug} & = &  \frac{\left(1 + b_{\rm drug}\right) E^\star_{\rm drug}}{b_{\rm drug}} \\
E^\star_{\rm drug} &\sim& {\rm N}\left(0, 10^6\right) \\
ET_{50,\rm drug} & = & \frac{t^\star}{b_{\rm drug}} \ ; \ \ t^\star  =  12\ {\rm weeks} \\
b_{\rm drug} & \sim & {\rm U}\left(0, 100\right)  \\
\gamma_{\rm drug} &\sim& {\rm truncated N} \left(0, 10^6\right), \ \gamma_{\rm drug} > 0 \mbox{ (enforce monotonicity w.r.t. dose)} 
\end{eqnarray*}

\begin{Sinput}
for(i in 1:(nDrugs)) {
  ## truncated Emax reparameterization to reduce autocorrelation
  logEStar[i] ~ dnorm(0, 1.0E-6)
  log(eStar[i]) <- logEStar[i]
  b[i] ~ dunif(0,100)
  eDelta[i] <- -(1 + b[i]) * eStar[i] / b[i]
  et50[i] <- tStar / b[i]
  gamma[i] ~ dnorm(0, 1.0E-6)I(0,)
}
\end{Sinput}


\subsection*{Common parameters}

\begin{eqnarray*}
\mu_{\alpha} &\sim& {\rm N}\left(0, 10^6\right)  \\
\log\left(AUC_{placebo}\right) & \sim & {\rm N}\left(0, 10^6\right) \\
k_{el} &\sim& {\rm U}\left(0, 2\right) \\
k_{eq} - k_{el} & \sim & {\rm U}\left(0, 2\right) \\
\beta & = & - AUC_{placebo} / \left( \frac{1}{k_{el}} - \frac{1}{k_{eq}} \right) \\
\mu_{\sigma} &\sim & {\rm U}\left(0, 10^4\right) \\
\alpha_\sigma & \sim &  U\left(0, 10^4\right) \\
\psi_{drug} & \sim & {\rm U}\left(0, 10^4\right) \\
\omega_{\rm intercept} &\sim & U\left(0, 10^4\right) \\ 
\psi_{\rm intercept} & \sim & U\left(0, 10^4\right) \\
df_{\rm intercept} & \sim & U\left(2, 100\right) \\
\omega_{\alpha} &\sim& U\left(0, 10^4\right) \\
\psi_{\alpha} & \sim &  U\left(0, 10^4\right) \\  
df_\alpha & \sim & U\left(2, 100\right) \\
\psi_{\rm drug} &\sim& U\left(0, 10^4\right)
\end{eqnarray*}

\begin{Sinput}

alphaMean ~ dnorm(0, 1.0E-6)
  
## reparameterization of placebo response in attempt to reduce autocorrelation
aucPlacebo ~ dunif(0, 100)
beta <- -aucPlacebo / (1/kel - 1/keq)
kel ~ dunif(0,2)
keqMinusKel ~ dunif(0,2)
keq <- kel + keqMinusKel

sigma ~ dunif(0, 1.0E4)
tauResid <- 1 / (sigma * sigma)
aResid ~ dunif(0, 1.0E4)
bResid <- aResid / tauResid

omegaInterceptStudy ~ dunif(0, 1.0E4)
omegaInterceptUnit ~ dunif(0, 1.0E4)
omegaAlphaStudy ~ dunif(0, 1.0E4)
omegaAlphaUnit ~ dunif(0, 1.0E4)
omegaDrug ~ dunif(0, 1.0E4)
dfInterceptUnit ~ dunif(2, 100)
dfAlphaUnit ~ dunif(2, 100)

tauInterceptStudy <- 1/(omegaInterceptStudy*omegaInterceptStudy)
tauInterceptUnit <- 1/(omegaInterceptUnit*omegaInterceptUnit)
tauAlphaStudy <- 1/(omegaAlphaStudy*omegaAlphaStudy)
tauAlphaUnit <- 1/(omegaAlphaUnit*omegaAlphaUnit)
tauDrug <- 1/(omegaDrug*omegaDrug)
\end{Sinput}

\end{document}