
## The actual data assembly script is maintained privately because it
## involves reading non-public data sets. In this modified version we
## just load an already assembled data set with all combined data.

load('../../data/derived/combined.RData')
