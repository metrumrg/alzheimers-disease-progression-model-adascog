

normAdas <- combined$Adas / 70
normAdas <- pmax(pmin(normAdas, 0.99), 0.01) # avoiding zeroes and ones to encourage better beta parameters (this affects only a few observations)

## put it all together
bugsdata <- list(
                 nStudies = length(unique(combined$Study)),
                 nUnits = length(unique(combined$Unit)),

                 nObsNat = sum(combined$Study2 == 'ADNI'),
                 nObsDrug = sum(combined$UnitN > 1), # studies for which we are using some drug data (the meta data)

                 nPat = combined$UnitN,
                 nPatUnit = combined$UnitN[!duplicated(combined$Unit)], 
                 
                 study = combined$Study,
                 studyUnit = combined$Study[!duplicated(combined$Unit)],

                 unit = combined$Unit,

                 nDrugs = length(unique(combined$Drug[combined$Drug > 0])),
                 drug = combined$Drug,
                 dose = combined$Dose,
                 
                 doseRef = unique(combined$DoseRef[match(1:3, combined$Drug)]),
                 tStar = 12, 
                 
                 time = combined$Week,

                 bmmse = combined$Bmmse[!duplicated(combined$Unit)],
                 meanBMMSE = 21, 
                 
                 normAdas = normAdas 

                 )


## makeBugsinitFun (defined below) is a function *to generate a
## function* to generate initial estimates. This extra level of
## indirection is necessary because the bugsinit function might get
## evaluated in a separate R session, where the values we want to use
## for nStudies, nUnits, etc, won't be bound to a variable; with the
## following strategy we put the values for these variables in the
## bugsinit function environment.


makeBugsinitFun <- function(nStudies, nUnits 
                            ) { 
  
  bugsinit <- function() {
    
    x1 <- list(
               ## overall population means: 
               nuAlpha = runif(1, 0, 0.05),
               nuEta = runif(1, -2, 0),
               lambdaEta = rnorm(1, -0.1, 0.1),
               lambdaAlpha = rnorm(1, 0, 0.5),
               
               ## placebo effect parameters:
               aucPlacebo = runif(1, 0, 6), 
               kel = runif(1, 0.013, 0.2), 
               keqMinusKel = runif(1, 0, 1),

               ## drug effect parameters: 
               gamma = runif(3, 0.5, 1.5),
               eStar = runif(3, 0, 0.3), 
               et50 = runif(3, 0, 100), 
               
               ## parameters related to residual distribution:
               phiEpsilon = runif(1, 0, 0.25),
               kappaEpsilon = runif(1, 1, 10), 

               ## parameters related to variance components 
               phiEta = runif(1, 0, 1),
               kappaEta = runif(1, 1, 10), 
               phiAlpha = runif(1, 0, 0.01),
               kappaAlpha = runif(1, 1, 10),  
               
               ## study-level variance components
               psiEta = runif(1, 0, 0.25),
               psiAlpha = runif(1, 0, 0.005) 
               
               )
      
    x2 <- list( 
               ## random effects:
               muEta = rnorm(nStudies, x1$nuEta, 0.1),
               muAlpha = rnorm(nStudies, x1$nuAlpha, 0.1),
               alpha = rnorm(nUnits, x1$nuAlpha, 0.1),
               eta = rnorm(nUnits, x1$nuEta, 0.1), 
               tau = rep(1/x1$phiEpsilon^2, nStudies),
               tauEta = rep(1/x1$phiEta^2, nStudies),
               tauAlpha = rep(1/x1$phiAlpha^2, nStudies)
               )

    inits <- c(x1, x2)
    return(inits)
    
  }

  return(bugsinit)
  
}

bugsinit <- makeBugsinitFun(nStudies = bugsdata$nStudies,
                            nUnits = bugsdata$nUnits)

## specify which variables to monitor (and which not to monitor)
initParNames <- names(bugsinit())
notMonitor <- NULL
notInitParNames <-  c("beta", "keq")
parameters <- c(setdiff(initParNames, notMonitor), notInitParNames)

## The following names reference long arrays of random variables, so
## you probably don't want to plot all of them:
notPlot <- c("deviance", "eta", "alpha", 'muEta',
             'muAlpha', 'tau', 'tauEta', 'tauAlpha')

parameters.to.plot <- setdiff(parameters, notPlot)


### End set-up objects for WinBUGS run
######################################################################
