\documentclass[12pt, reqno]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}

\usepackage{Sweave}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\oddsidemargin -0.5in
\evensidemargin -0.5in

\newcommand{\adasCfb}{\Delta \mathit{ ADAS}}
\newcommand{\adas}{\mathit{ ADAS}}


\begin{document}
% \section{}
% \subsection{}


\title{ADAS-cog progression model implemented as ``rawscoremodel''}
\maketitle

\parskip 12pt
\parindent 0em

\section*{Notation}

We index patients by $p$ and within each patient we index occasion by $i$. Where it is convenient, we will use $j = j(p)$, $k = k(p)$, and $d = d(p)$ to denote the indices of the study arm, study, and drug (if applicable) respectively, of patient $p$. Let $\adas_{ipk}$ denote the observed ADAS-cog score on the $i^{th}$ occasion in the $p^{th}$ patient in the $k^{th}$ study. We will indicated conditioning as follows:
\begin{itemize}
\item $[\, \cdot \, | \mbox{ study } k \,]$ indicates conditioning on all study-level random effects associated with study $k$, {\it without conditioning on patient-level random effects}. 
\item $[\, \cdot \, | \mbox{ patient } p \,]$ indicates conditioning on all patient-level random effects associated with patient $p$ {\it and all study-level random effects associated with study} $k(p)$. (Note that per our model hierarchy, conditioning on patient-level effects implies conditioning on the associated study-level random effects.)
\item $[\, \cdot \, | \mbox{ arm } j \,]$ indicates conditioning on all patient-level random effects for all patients in study arm $j$ {\it and all study-level random effects associated with that study}. That is, $[ \, \cdot \, | \mbox{ arm } j \,] \equiv [ \, \cdot \, | \{ \mbox{ patient } p : j(p) = j \} \,] $
\end{itemize}

\section*{Functions of fixed effect parameters}

\begin{eqnarray}
E_{{\rm placebo},ipk} &=& \beta \left(e^{-k_{el}t_{ijk}} - e^{-k_{eq}t_{ijk}}\right) \\
E_{{\rm drug},idk} &=& \left(\frac{D_{d}}{D_{{\rm ref},d}}\right)^{\gamma_{d}} 
	\frac{E_{\Delta,d} t_{idk}}{ET_{50,d} + t_{idk}} 
\end{eqnarray}

\begin{Sinput}
for(i in 1:nObsSummary) {

 ePlacebo[i] <- beta * (exp( -kel * time[i]) - exp( -keq * time[i] ))
 iDrug[i] <- max(1, drug[i])   # prevents syntax error when treatment = placebo
 eDrug[i] <- (1 - equals(drug[i], 0)) *
   pow(dose[i] / doseRef[ iDrug[i] ], gamma[ iDrug[i] ]) *
   eDelta[ iDrug[i] ] * time[i] / (et50[ iDrug[i] ] + time[i])
   
}
\end{Sinput}
  

\section*{Inter-study random effects}

\begin{eqnarray}
\alpha_{{\rm study}, k} & \sim & {\rm N} \left( \mu_{\alpha} , \psi^2_{\alpha} \right) \\
\eta_{{\rm intercept, study}, k} & \sim & {\rm N} \left( \mu_{\rm intercept } , \psi^2_{\rm intercept} \right) \\
\end{eqnarray}

\begin{Sinput}
for(i in 1:nStudies) {
  
    ## Study-level random effects:
    alphaStudy[i] ~ dnorm(muAlphaStudy, tauAlphaStudy)	
    etaInterceptStudy[i] ~ dnorm(muInterceptStudy, tauInterceptStudy)

}
\end{Sinput}


\section*{Inter-patient random effects}

\begin{eqnarray}
\alpha_{pk} \, | \mbox{ study } k &\sim& {\rm N} \left(\alpha_{{\rm study},k}, \, \omega^2_{\alpha} \right) \\
\eta_{{\rm intercept}, pk} \, | \mbox{ study } k &\sim& {\rm N} \left(\eta_{{\rm intercept, study},k}, \, \omega^2_{\rm intercept} \right) 
\end{eqnarray}

\begin{Sinput}
    
## In this implementation of the model (which uses summary 
## data only), inter-patient random effects are not directly 
## simulated.
    
\end{Sinput}

\section*{Distribution of individual patient scores}

\begin{eqnarray}
\adas_{ipk} / 70 \, | \mbox{ patient } p & \sim & {\rm Beta} \left( \theta_{ipk}  \tau_k    , \,    \left(  1 -  \theta_{ipk} \right) \tau_k  \right).
\end{eqnarray}

Note that this distribution is parameterized such that:

\begin{eqnarray}
{\rm E}[ \adas_{ipk} / 70 \, | \mbox{ patient } p ]  & = & \theta_{ipk} \\
{\rm V}[ \adas_{ipk} / 70 \, | \mbox{ patient } p ] & = & \frac{ \theta_{ipk} (1 - \theta_{ipk}) }{ \tau_k + 1}
\end{eqnarray}

We then model the conditional expectation as:

\begin{eqnarray}
\log \left[ \frac{\theta_{ipk}}{1 - \theta_{ipk}} \right]  & = &  \alpha_{pk} t_{ipk} + \eta_{{\rm intercept},pk} + E_{{\rm placebo},ipk} + E_{{\rm drug},ipk} 
\end{eqnarray}

\begin{Sinput}
    
## In this implementation of the model (which uses summary 
## data only), there are no observed individual patient 
## scores.
    
\end{Sinput}

\section*{Distribution of sample statistics}

Theoretically, the sample mean of Beta-distributed random variables would not itself be exactly Beta-distributed. However, the sample mean would be bounded between zero and one, so a Beta distribution is still a reasonable candidate to characterize its approximate distribution. The expectation of the sample mean equal to the average expectation of the individual Beta random variables, and (assuming conditional independence), the precision of the sample mean is equal to the sample size times the precision of one of the individual Beta random variables. We therefore model the conditional distribution of the sample mean as: 

\begin{eqnarray}
\overline{\adas}_{ijk} / 70 \, | \mbox{ arm } j & \sim &  {\rm Beta} \left( \overline{\theta}_{ijk}  (n_{jk} \tau_k + n_{jk} -1 )  , \,    \left(  1 -  \overline{\theta}_{ijk} \right) (n_{jk} \tau_k + n_{jk} -1 )  \right), \mbox{ where,} \\
\overline{\theta}_{ijk} & = & \frac{1}{n_{jk}}   \sum_{p : j(p) = j } \theta_{ipk} 
\end{eqnarray}


That is, $\overline{\theta}_{ijk}$ is the expected sample mean of $\adas / 70$ at time $i$ for arm $j$ in study $k$. To model $\overline{\theta}_{ijk}$, we first define the following terms: 

\begin{eqnarray}
\overline{\alpha}_{jk} & \equiv & \frac{1}{n_{jk}}   \sum_{p : j(p) = j } \alpha_{pk} \\
\overline{\eta}_{ {\rm intercept}, jk } & \equiv & \frac{1}{n_{jk}} \sum_{p : j(p) = j } \eta_{ {\rm intercept}, pk } \\
\overline{{\rm logit}[\theta]}_{ijk} & \equiv & \overline{\alpha}_{jk} t_{ijk} + \overline{\eta}_{ {\rm intercept}, jk } +  E_{{\rm placebo},ijk} + E_{{\rm drug},ijk}. 
\end{eqnarray}

We now model $\overline{\theta}_{ijk}$ using the following approximation, invoking the linearity of the logit function over the range of $\theta$ values of interest (refer to the section, {\it Linear approximation to the logit'}) :

\begin{eqnarray}
 {\rm logit}[\overline{\theta}]_{ijk}  & \approx & \overline{{\rm logit}[\theta]}_{ijk} \\ 
\end{eqnarray}

Note also that our model for $\overline{\adas}_{ijk} / 70$  results in approximately the correct gain in conditional precision, relative to a single observation:

\begin{eqnarray}
{\rm V}[ \overline{\adas}_{ijk} /70 \, | \mbox{ arm } j \,] & = & \frac{ \overline{\theta}_{ijk} ( 1 - \overline{\theta}_{ijk} ) }{ n_{jk} (\tau_k + 1) } \approx \frac{\overline{{\rm V}[ \adas_{ipk} | \mbox{ patient } p \,]} }{ n_{jk}}
\end{eqnarray}

\begin{Sinput}
 for(i in 1:nObsSummary) { 
   
   ## Wihin-unit variation (conditional on unit and study) 

   normAdas[i] ~ dbeta(aAdas[i], bAdas[i]) 
   
   aAdas[i] <-  theta[i] * tauResid[i] + 1.0/(1.0E6) 
   bAdas[i] <- (1 - theta[i]) * tauResid[i] + 1.0/(1.0E6) 
   
   syscomp[i] <-  alpha[ unit[i] ] * time[i] + etaIntercept[ unit[i] ]  + ePlacebo[i] + eDrug[i]
   theta[i] <- 1 / (1 + exp(-syscomp[i]) ) # conditional expectation for subject
   
   ## This adjustment results in an nPat-fold increase in the
   ## precision of the beta distribution (relative to single-patient
   ## precision) :
   tauResid[i] <- tauResidStudy * nPat[i] + nPat[i]  - 1
}
  
\end{Sinput}


\subsection*{``Shorthand'' random effects}

In theory one could simulate all of the patient-level random effects $\alpha_{pk}$ and $\eta_{ {\rm intercept}, pk }$ that are required to compute $\overline{\alpha}_{jk}$ and $\overline{\eta}_{ {\rm intercept}, jk}$. This would be extravagant, since we would be simulating all of those random effects only to compute their mean, whose distribution should be well-approximated by the Central Limit Theorem (as long as each arm consists of a reasonable number of patients). Accordingly, model these ``shorthand'' random effects directly, according to their asymptotic distributions:

\begin{eqnarray}
\overline{\alpha}_{jk} \, | \mbox{ study } k & \sim & {\rm N}( \alpha_{ {\rm study}, k}, \, \omega^2_{\alpha} / n_{jk} )\\
\overline{\eta}_{ {\rm intercept}, jk } \, | \mbox{ study } k & \sim & {\rm N}( \eta_{ {\rm intercept, study}, k}, \, \omega^2_{\rm intercept} / n_{jk} ) 
\end{eqnarray}

\begin{Sinput}
  for(i in 1:nUnits) {

    ## Between-unit variation, conditional on study :
    
    ##  "eta-bar" in model description in slides (or just eta, if n = 1)
    etaIntercept[i] ~ dnorm(etaInterceptStudy[ studyUnit[i] ], tauInterceptUnitAdj[i])
    tauInterceptUnitAdj[i] <- tauInterceptUnit * nPatUnit[i]
    ##  "alpha-bar" in model description in slides (or just alpha, if n = 1)
    alpha[i] ~ dnorm(alphaStudy[ studyUnit[i] ], tauAlphaUnitAdj[i])
    tauAlphaUnitAdj[i] <- tauAlphaUnit * nPatUnit[i]

 }
\end{Sinput}

\section*{Priors}

\subsection*{Parameters varying by drug}

\begin{eqnarray}
E_{\Delta,\rm drug} & = &  \frac{\left(1 + b_{\rm drug}\right) E^\star_{\rm drug}}{b_{\rm drug}} \\
\log(E^\star_{\rm drug}) &\sim& {\rm N}\left(0, 0.1 \right) \\
ET_{50,\rm drug} & = & \frac{t^\star}{b_{\rm drug}} \ ; \ \ t^\star  =  12\ {\rm weeks} \\
b_{\rm drug} & \sim & {\rm U}\left(0, 100\right)  \\
\gamma_{\rm drug} &\sim& {\rm U} \left(0.01, 10 \right)
\end{eqnarray}

\begin{Sinput}
  for(i in 1:(nDrugs)) {
    ## truncated Emax reparameterization to reduce autocorrelation
    logEStar[i] ~ dnorm(0, 0.1) # re:precision, note *log*
    log(eStar[i]) <- logEStar[i]
    b[i] ~ dunif(0.01, 100)
    eDelta[i] <- -(1 + b[i]) * eStar[i] / b[i]
    et50[i] <- tStar / b[i]
    gamma[i] ~ dunif(0.01, 10)
  }  
\end{Sinput}


\subsection*{Common parameters}

\begin{eqnarray}
\mu_{\alpha} &\sim& {\rm N}\left(0, 100\right)  \\
\mu_{\rm intercept} &\sim& {\rm N}\left(0, 100\right)  \\
AUC_{placebo} & \sim & {\rm U}\left(0, 100\right) \\
\beta & = & - \mathit{AUC}_{\rm placebo} / \left( \frac{1}{k_{el}} - \frac{1}{k_{eq}} \right) \\
k_{el} &\sim& {\rm U}\left(0, 2\right) \\
k_{eq} - k_{el} & \sim & {\rm U}\left(0, 2\right) \\
\psi_{\rm intercept} & \sim & {\rm U}\left(0, 10\right) \\
\omega_{\rm intercept} &\sim & {\rm U}\left(0, 10\right) \\ 
\psi_{\alpha} & \sim &  {\rm U}\left(0, 10\right) \\  
\omega_{\alpha} &\sim& {\rm U}\left(0, 10\right) \\
\mu_{\sigma} &\sim & {\rm U}\left(0, 1000\right) \\
\mu_{\tau} & = & 1  / \mu^2_{\sigma} \\
\tau & = & \mu_{\tau} 
\end{eqnarray}

\begin{Sinput}
  muAlphaStudy ~ dnorm(0, 1.0E-2) 
  muInterceptStudy ~ dnorm(0, 1.0E-2)
  ## reparameterization of placebo response in attempt to reduce autocorrelation
  aucPlacebo ~ dunif(0, 100)
  beta <- -aucPlacebo / (1/kel - 1/keq)
  kel ~ dunif(0,2)
  keqMinusKel ~ dunif(0,2)
  keq <- kel + keqMinusKel
 
  ## Variance components ... 
  omegaInterceptStudy ~ dunif(0, 10)
  omegaInterceptUnit ~ dunif(0, 10)
  omegaAlphaStudy ~ dunif(0, 10)
  omegaAlphaUnit ~ dunif(0, 10)
q()  ## ... and associated precisions
  tauInterceptStudy <- 1/(omegaInterceptStudy*omegaInterceptStudy)
  tauInterceptUnit <- 1/(omegaInterceptUnit*omegaInterceptUnit)
  tauAlphaStudy <- 1/(omegaAlphaStudy*omegaAlphaStudy)
  tauAlphaUnit <- 1/(omegaAlphaUnit*omegaAlphaUnit)

  muSigmaStudy ~ dunif(0, 1.0E3) # note this "sigma" is not the SD of the resids (although it is related)
  muTauStudy <- 1 / (muSigmaStudy * muSigmaStudy)
  tauResidStudy <- muTauStudy # redundant parameter, but I've already run the model with these variable names :-)
  

\end{Sinput}

\section*{Linear approximation to the logit} 

The theoretical justification for the model of expected sample means relies on the approximate linearity of the the logit transformation over the range of interest. The first-order Taylor approximation of ${\rm logit}(x)$ (expanding around $x = 0.5$) is $-2 + 4x$. Figure \ref{fig:logitApprox} shows that this is an extremely good approximation for the range of ADAS-cog scores that are observed in the trials being modeled. While the figure shows the {\it observed} sample means, note that the approximation is only invoked for the {\it expected} sample means (i.e. the $\overline{\theta}_{ijk}$). Since the latter is mathematically guaranteed to have a smaller variance, the quality of the approximation would in fact be even better than what is suggested by the figure. 

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.7]{logitApprox.pdf}
\caption[]{Logit-transformed ADAS-cog scores versus ADAS-cog scores on original scale for four studies. All sample means for all treatments and all time points are shown. For mean scores in this range, the logit transformation (shown as solid line) is extremely well approximated by the linear transformation $y = -2 + 4 * (\adas / 70)$, shown as a dashed line. \label{fig:logitApprox} }
\end{center}
\end{figure}

In addition to using this approximation to justify our model, we can also use the approximation to interpret the fitted parameter values. For example, if $\hat{\mu}_{\alpha}$ is an estimate of the model parameter $\mu_{\alpha}$, then the estimated natural rate of decline (on the original scale) would be approximately $70 * \hat{\mu}_{\alpha} / 4$. Similar transformations can be used to interpret other model parameters. 

\end{document}