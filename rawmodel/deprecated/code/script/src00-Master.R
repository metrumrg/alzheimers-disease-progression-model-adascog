######################################################################
### User configuration

projectDir <- switch(Sys.info()['user'],
                     jimr = switch(Sys.info()['nodename'],
                       dirname(dirname(dirname(getwd()))) # if R launched from the directory holding THIS script
                       ),
                     dirname(dirname(dirname(getwd()))) # if R launched from the directory holding THIS script
                     )

## workDir must contain model file and all scripts; also, all output
## will be put in workDir or subdirectories of workDir
workDir <- file.path(projectDir, "code/script/")

## See src02-LoadData.R for files that are expected to be in dataDir
dataDir <- file.path(projectDir, "data/derived")

## Contents of toolsDir can be obtained from Google code site for bugsparallel.
## http://code.google.com/p/bugsparallel/
## toolsDir <- file.path(projectDir, "code/utilities")
toolsDir <- "/Users/jimr/myGoogleCode/bugsparallel/code"

resourceOption <- switch(Sys.info()["nodename"],
                         vega.local = "wine-single",
                         "metrum-host01.managed.contegix.com" = "wine-parallel",
                         "metrum-host02.managed.contegix.com" = "wine-parallel",
                         "fin.local" = "wine-parallel", 
                         BLOOM = "windows-parallel",
                         "METRUM-4D795D45" = "windows-parallel",
                         "windows-single")

## ~/wine-1.0.1/wine ~/.wine/drive_c/Program\ Files/WinBUGS14/WinBUGS14.exe

bugsDir <- switch(Sys.info()['user'],
                  jimr = switch(Sys.info()["nodename"],
                    vega.local = path.expand(paste("~/.wine/drive_c/" ,"Program Files/WinBUGS14/",sep="")),
                    "metrum-host01.managed.contegix.com" = path.expand(paste("~/.wine/drive_c/" ,"Program Files/WinBUGS14/",sep="")),
                    "metrum-host02.managed.contegix.com" = path.expand(paste("~/.wine/drive_c/" ,"Program Files/WinBUGS14/",sep="")),
                    "fin.local" = path.expand(paste("~/.wine/drive_c/" ,"Program Files/WinBUGS14/",sep="")),
                    BLOOM = "C:/Program Files (x86)/WinBUGS14",
                    "C:/Program Files/WinBUGS14"
                    ),
                  "C:/Program Files/WinBUGS14" # if typical Windows install
                  )

## The following only need to be specified if running over wine:

WINE <- switch(Sys.info()['user'],
               jimr = switch(Sys.info()["nodename"],
                 vega.local = "~/wine-1.0.1/wine", 
                 "metrum-host01.managed.contegix.com" = "~/quickstart/quickwinenoauth.pl",
                 "metrum-host02.managed.contegix.com" = "~/quickstart/quickwinenoauth.pl"
                 ),
               "/Applications/Darwine/Wine.bundle/Contents/bin/wine"
               )

WINEPATH <- switch(Sys.info()['user'],
                   jimr = switch(Sys.info()["nodename"],
                     vega.local = "~/wine-1.0.1/programs/winepath/winepath", 
                     "metrum-host01.managed.contegix.com" = "~/wine-1.0.1/programs/winepath/winepath",
                     "metrum-host02.managed.contegix.com" = "~/wine-1.0.1/programs/winepath/winepath"
                     ),
                   "/Applications/Darwine/Wine.bundle/Contents/bin/winepath"
                   )
               



### End user configuration
######################################################################


######################################################################
### Script options

model.name <- "rawscoremodel" # root name of BUGS model file

set.seed(10271998) # not required but assures repeatable results

## WinBUGS options !!! PROVIDED VALUES ARE FOR TESTING ONLY - CHANGE
## ME TO RECOMMENDED VALUES OR SIMILAR !!!

n.chains <- 4    # recommend ~ 4
n.iter <- 50000     # recommend ~ 5e4
n.burnin <- 5000   # recommend ~ 5e3
n.thin <- 25      # recommend ~ 25

### End script options
######################################################################


######################################################################
### Generic configuration (shouldn't usually require editing)

options(stringsAsFactors = FALSE)

library(coda)
library(lattice)
if(Sys.info()['nodename'] == "vega.local") library(gdata)
if(Sys.info()['nodename'] == "vega.local") library(RColorBrewer)
if(resourceOption %in% c("windows-parallel", "wine-parallel")) library(Rmpi)
  
source(file.path(toolsDir, "bugs.tools.R"))
source(file.path(toolsDir, "bgillespie.utilities.R"))
source(file.path(toolsDir, "bugsParallel.R"))
source(file.path(workDir, "myR2WinBUGS.R"))

### End generic configuration
######################################################################

######################################################################
### Release the hounds:
 
source(file.path(workDir, 'src01-First.R'))
source(file.path(workDir, 'src02-LoadData.R')) # warning message is for CI's that get set to NA
source(file.path(workDir, 'src03-RunBugs.R'))
source(file.path(workDir, 'src04-IndPred.R'))
source(file.path(workDir, 'src05-PopPred.R'))
source(file.path(workDir, 'src06-MiscModelCheck.R'))

### The end.
######################################################################

