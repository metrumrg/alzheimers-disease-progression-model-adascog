#' Calculate the CDR global score from a dataframe of individual CDR scores
#' 
#' @param df A dataframe of CDR item scores (must be named CDRM, CDRO, CDRJ, CDRC, CDRH, CDRP)
#' @return The CDR Global score as defined by Morris, 1993 in Neurology
#' @examples 
#' cdrglobal(data.frame(CDRM=2, CDRO=1, CDRJ=2, CDRC=3, CDRH=0.5, CDRP=0))

cdrglobal <- function(df){
  library(metrumrg)
  `%nin%` <- function(x, table){
    match(x, table, nomatch=0) == 0
  }
  if(all(names(df) %nin% c("CDRM","CDRO","CDRJ","CDRC","CDRH","CDRP"))){
    return(print("Pass in named vector (CDRM, CDRO, CDRJ, CDRC, CDRH, CDP)"))
  }
  df <- metrumrg::shuffle(df,"CDRM")
  if(class(df[,1])=="factor") df <- as.best(df)
  
  
  #df$CDRG <- NA
  CDRG <- vector(mode="numeric", length=nrow(df))
  for(i in 1:length(CDRG)){
    if(any(is.na(df[i,]))){
      CDRG[i] <- NA
    }else{
      CDRM <- df[i,1]
      if( (CDRM==0.5) &  (sum(df[i,-1]>=1)>=3)){
        CDRG[i] <- 1
      }
      if( sum(df[i,-1]==CDRM)>=3) {
        CDRG[i] <- CDRM
      }else{
        gt <- df[i,-1][df[i,-1] > CDRM]
        names(gt) <- names(df[i,-1])[df[i,-1] > CDRM]
        lt <- df[i,-1][df[i,-1] < CDRM]
        names(lt) <- names(df[i,-1])[df[i,-1] < CDRM]
        if((length(gt)==3 & length(lt)==2) | 
           (length(gt)==2 & length(lt)==3) ){
          CDRG[i] <- CDRM
        }else{
          if(length(gt)>=3){
            if(sum(table(gt)==sort(table(gt),decreasing=T)[1])>1){
              ties <- gt[ as.character(gt) %in% names(table(gt)[table(gt)==max(table(gt))]) ]
              CDRG[i] <- unique(ties[ which( abs(CDRM-ties) == min(abs(CDRM-ties))) ])
            }else{
              CDRG[i] <- as.numeric(names( sort(table(gt),decreasing=T) )[1])
            }
          }else if(length(lt)>=3){
            if(sum(table(lt)==sort(table(lt),decreasing=T)[1])>1){
              ties <- lt[ as.character(lt) %in% names(table(lt)[table(lt)==max(table(lt))]) ]
              CDRG[i] <- unique(ties[ which( abs(CDRM-ties) == min(abs(CDRM-ties))) ])
            }else{
              CDRG[i] <- as.numeric(names( sort(table(lt),decreasing=T))[1])
            }
          }else{
            CDRG[i] <- CDRM
          }
        }
      }
      if(CDRM==0.5 & CDRG[i]==0) CDRG[i] <- 0.5
      if(CDRM==0){
        if(sum(df[i,-1]>=0.5)>=2) CDRG[i] <- 0.5 else CDRG[i] <- 0
      } 
    }
  }
  CDRG
}