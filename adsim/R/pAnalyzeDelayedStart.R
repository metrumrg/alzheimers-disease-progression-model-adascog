pAnalyzeDelayedStart <-
function(dat, ep.time, stab.time) {
  require(nlme)
  require(contrast)
  baseline <- subset(dat, Time == 0)
  ind <- match(dat$Patient, baseline$Patient)
  dat$Baseline <- baseline$AdasCog[ind]
  dat$CBL <- with(dat, AdasCog - Baseline)
  dat$Arm <- factor(dat$Arm)
  dat <- subset(dat, Time > 0)
  dat$Time2 <- factor(dat$Time)
  dat$Visit <- as.numeric(dat$Time2)
  conList <- lapply(1:length(ep.time),        
                    function(i) {
                      ti <- ep.time[i]
                      dati <- subset(dat, Time <= ti)
                      modi <- gls(CBL ~ Arm * Time2 + Baseline,
                                  data = dati,
                                  correlation = corSymm(form = ~ Visit | Patient)
                                  )
                      coni <- contrast(modi,
                                       a = list(Arm = "B", Baseline = mean(dati$Baseline), Time2 = paste(ti)),
                                       b = list(Arm = "A", Baseline = mean(dati$Baseline), Time2 = paste(ti)),
                                       type = "individual"
                                     )
                      coni
                    }
                    )
  conList <- lapply(conList, function(coni) unlist(coni[c(3:7, 9)]))
  for(i in seq(conList)) names(conList[[i]]) <- paste(names(conList[[i]]), ep.time[i], sep = ".")

  dat.stab <- subset(dat, Time %in% stab.time)
  mod.stab <- gls(CBL ~ Arm * Time2 + Baseline,
                  data = dat.stab,
                  correlation = corSymm(form = ~ 1 | Patient)
                  )
  npars <- length(coef(mod.stab))
  est.stab <- coef(mod.stab)[npars]
  ci.stab <- confint(mod.stab, level = 0.9)[npars, ]
  
  res <- do.call('c', conList)
  res <- c(res, est.stab, ci.stab)
  res
}

